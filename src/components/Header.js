import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import "../hamburgers.css";

export default class Header extends Component {
    state = {
        showMenu: false
    };

    menuTrigger = () => {
        this.setState(prevState => ({
            showMenu: !prevState.showMenu
        }));
    };

    langTrigger = () => {
        this.setState(prevState => ({
            showLang: !prevState.showLang
        }));
    };

    render() {
        const menu = this.state.showMenu ? "is-active" : "";
        const lang = this.state.showLang ? "is-active" : "";

        return (
            <header>
                <div className="header-inner section-inner">
                    <div className="logo">
                        <NavLink exact to="/">Cztery <span>是波蘭文的「四」</span></NavLink>
                    </div>
                    <nav className={this.state.showMenu ? "is-active" : ""}>
                        <ul>
                            <li>
                                <NavLink
                                    onClick={this.menuTrigger}
                                    activeClassName="is-active"
                                    exact
                                    to="/"
                                >
                                    首頁
                                </NavLink>
                            </li>
                            <li>
                                <NavLink
                                    onClick={this.menuTrigger}
                                    activeClassName="is-active"
                                    to="feng-chia"
                                >
                                    逢甲八景
                                </NavLink>
                            </li>
                            <li>
                                <NavLink
                                    onClick={this.menuTrigger}
                                    activeClassName="is-active"
                                    to="taichung"
                                >
                                    台中八景
                                </NavLink>
                            </li>
                            <li className="lang" onClick={this.langTrigger}>
                                英文版
                            </li>
                        </ul>
                    </nav>
                    <button
                        className={"hamburger hamburger--collapse " + menu}
                        onClick={this.menuTrigger}
                        type="button"
                    >
                        <span className="hamburger-box">
                            <span className="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
                <div className={'alert ' + lang}><span>抱歉！英文，我們看不懂！</span> <div className="btn" onClick={this.langTrigger}>好</div></div>
            </header>
        );
    }
}
