import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../pages/Home';
import Taichung from '../pages/Taichung';
import FengChia from '../pages/FengChia';

const Main = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/taichung" component={Taichung} />
        <Route exact path="/feng-chia" component={FengChia} />
      </Switch>
    </div>
  );
};

export default Main;