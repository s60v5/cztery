import React from "react";

const Footer = () => {
    return (
        <div>
            <footer>
                <div className="section-inner">
                    <div>
                        <p>數位時空旅人</p>
                    </div>
                    <div>
                        <p>教育部「數位人文計畫」</p>
                        <p>逢甲大學中文系「台灣人文概論」</p>
                    </div>
                </div>
            </footer>
            <subfooter>
                <div className="section-inner">
                    <div className="react icon">
                        <a href="https://zh-hant.reactjs.org/" rel="noopener noreferrer" target="_blank">
                            Made with React
                            <img src={require("../img/react.svg")} alt="React" />
                        </a>
                    </div>
                </div>
            </subfooter>
        </div>
    );
};

export default Footer;
