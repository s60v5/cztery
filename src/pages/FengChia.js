import React, { Component } from "react"
import Swiper from "react-id-swiper"
import "swiper/css/swiper.css"

const imageUrl = [
    require(`../img/feng-chia/1.jpg`),
    require(`../img/feng-chia/2.jpg`),
    require(`../img/feng-chia/3.jpg`),
    require(`../img/feng-chia/4.jpg`),
    require(`../img/feng-chia/5.jpg`),
    require(`../img/feng-chia/6.jpg`),
    require(`../img/feng-chia/7.jpg`),
    require(`../img/feng-chia/8.jpg`),
]

const slides = [
    {
        title: "蓮影塘韻",
        content: "春曦落滿塘，小船駐蓮間。<br />東風亂雲影，鴨啼盪水邊。",
    },
    {
        title: "踏石前行",
        content:
            "錯落的石板路上，灑滿了入秋的金黃，<br />看不見盡頭的不是那路，而是我對你綿長的思念。<br />一腳踏入，便再也找不著歸途。",
    },
    {
        title: "曦映穿林",
        content: "林蔭染百草，戔戔人影疏。<br />日華見彩雲，漫漫石間路。",
    },
    {
        title: "瑟夜浮燈",
        content:
            "夜風瑟瑟竄人心，<br />盼君歸來賞玉蟬。<br />切莫待君回首時，<br />方知佳人已離散",
    },
    {
        title: "林隙窺日",
        content:
            "春天剛降臨逢甲大學，好一幅悠閒的風景。<br />樹葉在微風中搖曳著，太陽在天空中隱藏。",
    },
    {
        title: "駐蜓聽荷",
        content:
            "日照渠塘發野蓮，<br />游魚戲水於蓮間。<br />小荷尖尖始發華，<br />卻見紅蜓駐蓮端。",
    },
    {
        title: "撥枝見秋",
        content: "綠葉茸茸，仙境匿其中。<br />朝曦點點，林蔭詠早秋。",
    },
    {
        title: "綠荷發華",
        content: "蔚藍蒼穹，樹影婆娑。<br />荷葉田田，百里生花。",
    },
]

export default class FengChia extends Component {
    state = {
        mapPopup: false,
        picPopup: false,
        picUrl: "",
        picAlt: ""
    }

    toggleMap = () => {
        this.setState((prevState) => ({
            mapPopup: !prevState.mapPopup,
        }))
    }

    openPic = (url, alt) => {
        this.setState({ picPopup: true })
        this.setState({ picUrl: url, picAlt: alt })
    }

    closePic = () => {
        this.setState({ picPopup: false })
    }

    render() {
        const params = {
            init: true,
            loop: true,
            speed: 800,
            slidesPerView: "auto",
            centeredSlides: true,
            autoHeight: true,
            effect: "coverflow", // 'cube', 'fade', 'coverflow',
            coverflowEffect: {
                rotate: 50, // Slide rotate in degrees
                stretch: 0, // Stretch space between slides (in px)
                depth: 100, // Depth offset in px (slides translate in Z axis)
                modifier: 1, // Effect multipler
                slideShadows: true, // Enables slides shadows
            },
            grabCursor: true,
            parallax: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            breakpoints: {
                980: {
                    autoHeight: false,
                },
            },
        }

        const map = this.state.mapPopup ? "is-active" : ""
        const pic = this.state.picPopup ? "is-active" : ""

        return (
            <div>
                <div class={"mapPopup " + map}>
                    <div class="popup-close" onClick={this.toggleMap}>
                        &times;
                    </div>
                    <iframe
                        src="https://www.google.com/maps/d/u/0/embed?mid=1So8o_IN9-pWP4yIYQqmKkjUe7SimQsz9&z=17"
                        width="100%"
                        height="100%"
                        title="Feng Chia Google Map"
                    ></iframe>
                </div>
                <div class={"picPopup " + pic}>
                    <div class="popup-close" onClick={this.closePic}>
                        &times;
                    </div>
                    <img src={this.state.picUrl} alt={this.state.picAlt} />
                </div>
                <section className="color">
                    <div className="section-inner center-vertically padding-medium">
                        <h2 className="title-1 margin-bottom-0">傾逢八景</h2>
                        <p className="text-center">
                            我們以新生的身分走訪了大學裡的無數角落，並從中精挑細選了八張「靜照」
                            <br />
                            是的！我們所側重的焦點是 — 逢甲校園的「寧靜與清幽」
                            <br />
                            <br />
                            也希望初來乍到的人們，能夠藉由照片中我們的視角
                            <br />
                            感受到逢甲並不只帶給人們忙碌的印象，而是一間可熱鬧可幽靜的美麗學院
                            <br />
                            <br />
                            且「逢」字亦有遇見之意
                            <br />
                            於是，我們將之命名為「傾逢八景」
                            <br />
                            <br />
                            意即：「初遇逢甲，便一見傾心！」
                        </p>
                    </div>
                </section>
                <section>
                    <div className="inner-section padding-big">
                        <Swiper {...params}>
                            {slides.map((slide, index) => {
                                return (
                                    <div
                                        className="swiper-slide"
                                        style={{
                                            backgroundImage: `url(${imageUrl[index]})`,
                                        }}
                                    >
                                        <img src={imageUrl[index]} alt={slide.title}/>
                                        <div className="content">
                                            <p
                                                className="title"
                                                data-swiper-parallax="-30%"
                                                data-swiper-parallax-scale=".7"
                                            >
                                                {slide.title}
                                            </p>
                                            <span
                                                className="caption"
                                                data-swiper-parallax="-20%"
                                            >
                                                <div dangerouslySetInnerHTML={{ __html: slide.content}} />
                                            </span>
                                            <div class="actions">
                                                <div
                                                    class="open-map"
                                                    onClick={this.toggleMap}
                                                >
                                                    地圖
                                                </div>
                                                <div
                                                    class="open-pic"
                                                    onClick={() =>
                                                        this.openPic(
                                                            imageUrl[index],
                                                            slide.title
                                                        )
                                                    }
                                                >
                                                    全解析度圖片
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </Swiper>
                    </div>
                </section>
            </div>
        )
    }
}
