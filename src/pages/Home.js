import React, { Component } from "react";

export default class Home extends Component {
    state = {
        text: ""
    };

    typeWriter = (text, i, fnCallback) => {
        if (i < text.length) {
            this.setState({ text: text.substring(0, i + 1) });
            setTimeout(() => {
                this.typeWriter(text, i + 1, fnCallback);
            }, 250);
        } else if (typeof fnCallback == "function") {
            // call callback after timeout
            setTimeout(fnCallback, 700);
        }
    };

    startAnimation = (n, dataText) => {
        if (typeof dataText[n] == "undefined") {
            setTimeout(() => {
                this.startAnimation(0, dataText);
            }, 5000);
        } else {
            this.typeWriter(dataText[n], 0, () => {
                // after callback (and whole text has been animated), start next text
                this.startAnimation(n + 1, dataText);
            });
        }
    };

    componentDidMount() {
        const dataText = ["我們是第四組", "Cztery", "Four", "四四四四"];

        this.startAnimation(0, dataText);
    }

    render() {
        return (
            <main>
                <section className="height50 color">
                    <div className="section-inner">
                        <div className="cztery">
                            <div>
                                <p id="typeWriter">{this.state.text}</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="padding-top">
                    <div className="section-inner section-fullwidth-mobile">
                        <div className="row">
                            <div className="news">
                                <img
                                    src={require("../img/homepage_1.jpg")}
                                    alt="團隊成員"
                                />
                                <div className="news-inner">
                                    <h2>團隊成員</h2>
                                    <p>
                                        我們是逢甲大學中文一的學生。
                                        <br />
                                        「我是超ㄎㄧㄤ台中人，勵志當一條鹹魚。」——組長簡繹純{" "}
                                        <br />
                                        「喜靜，愛咖啡與甜食，沉迷吸貓。」——羅郁鈞{" "}
                                        <br />
                                        「我來自波蘭。我之後想要養一隻柴犬。」——柯大衛{" "}
                                        <br />
                                        「我喜歡甜點和海。」——黃閔琪 <br />
                                        「我來自台中，夢想是寫出有溫度的文字。」——林志興
                                    </p>
                                </div>
                            </div>
                            <div className="news">
                                <img
                                    src={require("../img/homepage_2.jpg")}
                                    alt="踏查歷程"
                                />
                                <div className="news-inner">
                                    <h2>踏查歷程</h2>
                                    <p>
                                        在校園裡，我們沒有按照特定的路線，只是隨意地逛著，並一邊拍攝記錄。
                                        <br />
                                        而在校外踏查時，可能是一面聽著導覽人員訴說著景物的背景與歷史，一面拍攝照片同時，就好像整個人都進入了一種沉靜的氛圍似的，令人難以忘懷。
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="news">
                                <img
                                    src={require("../img/homepage_3.jpg")}
                                    alt="沿途心得"
                                />
                                <div className="news-inner">
                                    <h2>沿途心得</h2>
                                    <p>
                                        我們學會如何放緩腳步，用雙眼去記錄一切細微而美麗的世界。
                                    </p>
                                </div>
                            </div>
                            <div className="news">
                                <img
                                    src={require("../img/homepage_4.jpg")}
                                    alt="團隊成員"
                                />
                                <div className="news-inner">
                                    <h2>團隊成員</h2>
                                    <p>
                                        拍攝照片：簡繹純
                                        <br />
                                        八景命名：羅郁鈞
                                        <br />
                                        八景內文：簡繹純、林志興、黃閔琪、柯大衛
                                        <br />
                                        地圖標示：柯大衛
                                        <br />
                                        簡報製作：簡繹純、羅郁鈞、柯大衛
                                        <br />
                                        網頁製作：羅郁鈞、柯大衛
                                        <br />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        );
    }
}
