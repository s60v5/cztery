import React, { Component } from "react";
import Swiper from "react-id-swiper";
import "swiper/css/swiper.css";

const imageUrl = [
    require(`../img/taichung/1.jpg`),
    require(`../img/taichung/2.jpg`),
    require(`../img/taichung/3.jpg`),
    require(`../img/taichung/4.jpg`),
    require(`../img/taichung/5.jpg`),
    require(`../img/taichung/6.jpg`),
    require(`../img/taichung/7.jpg`),
    require(`../img/taichung/8.jpg`)
];

const slides = [
    {
        'title': '橋湖映月',
        'content': '獨木傾身照佳顏，<br />白衣書生憩於榮。<br />拱橋曲腰映綠水，<br />紅裙錦鱗戲其中。'
    },
    {
        'title': '暉落望月',
        'content': '朝曦透疏林，疏林遮望月。<br />望月藏青巒，青巒立朝曦。'
    },
    {
        'title': '靜樓觀月',
        'content': '鼓樓的想，<br />朝晨的念。<br />襦裙掃過台階上的薄灰，<br />遠眺是在每個風雨後的長夜，<br />成了每塊磚瓦上，<br />數不清的斑駁歲月。'
    },
    {
        'title': '奔馬朝月',
        'content': '風兒輕，陽光柔。<br />古銅色肌膚閃耀。<br />馬兒，你怎停了蹄。'
    },
    {
        'title': '拾級尋跡',
        'content': '烈日灼灼，<br />綠樹蔭蔭，<br />步石階而行，<br />只為尋你足跡。'
    },
    {
        'title': '緣源茶盞',
        'content': '飲一杯茶水，思一壺源。<br />觀百世人生，念半生緣。'
    },
    {
        'title': '恍臨舊夢',
        'content': '濃濃的花生味， 自路旁古玩店飄出。<br />門前的兩條狗兒， 你們各自在等著誰呢？'
    },
    {
        'title': '夕暉傍碑',
        'content': '落日斜陽，櫟社二十。 <br />淬礪品德，傳承漢學。'
    }
]

const mainMapUrl = require(`../img/tc_map.jpg`)

export default class Taichung extends Component {
    state = {
        mapPopup: false,
        picPopup: false,
        mainMapPopup: false,
        picUrl: '',
        picAlt: ''
    };

    toggleMap = () => {
        this.setState(prevState => ({
            mapPopup: !prevState.mapPopup
        }));
    };

    toggleMainMap = () => {
        this.setState(prevState => ({
            mainMapPopup: !prevState.mainMapPopup
        }));
    };

    openPic = (url, alt) => {
        this.setState({picPopup: true})
        this.setState({picUrl: url, picAlt: alt})
    }

    closePic = () => {
        this.setState({picPopup: false})
    }

    render() {
        const params = {
            init: true,
            loop: true,
            speed: 800,
            slidesPerView: "auto",
            centeredSlides: true,
            autoHeight: true,
            effect: "coverflow", // 'cube', 'fade', 'coverflow',
            coverflowEffect: {
                rotate: 50, // Slide rotate in degrees
                stretch: 0, // Stretch space between slides (in px)
                depth: 100, // Depth offset in px (slides translate in Z axis)
                modifier: 1, // Effect multipler
                slideShadows: true // Enables slides shadows
            },
            grabCursor: true,
            parallax: true,
            pagination: {
                el: ".swiper-pagination",
                clickable: true
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev"
            },
            breakpoints: {
                980: {
                    autoHeight: false
                }
            }
        };

        const map = this.state.mapPopup ? "is-active" : "";
        const pic = this.state.picPopup ? "is-active" : "";
        const mainMap = this.state.mainMapPopup ? "is-active" : "";

        return (
            <div>
                <div class={"mapPopup " + map}>
                    <div class="popup-close" onClick={this.toggleMap}>&times;</div>
                    <iframe
                        src="https://www.google.com/maps/d/u/0/embed?mid=1oSN11SVMEK9x48ech-FQr5K0zjXcx1-D&z=13"
                        width="100%"
                        height="100%"
                        title="Taichung Google Map"
                    ></iframe>
                </div>
                <div class={"picPopup " + mainMap}>
                    <div class="popup-close" onClick={this.toggleMainMap}>&times;</div>
                    <img src={mainMapUrl} alt="Taichung Map" />
                </div>
                <div class={"picPopup " + pic}>
                    <div class="popup-close" onClick={this.closePic}>&times;</div>
                    <img src={this.state.picUrl} alt={this.state.picAlt} />
                </div>
                <section className="color">
                    <div className="section-inner center-vertically padding-medium">
                        <h2 className="title-1 margin-bottom-0">
                            台中「心」八景
                        </h2>
                        <p className="text-center">
                            我們實地走訪了四個景點 <br />
                            台中公園、第二市場<br />林家花園宮保第、明台高中萊園
                        </p>
                        <div class="open-main-map" onClick={this.toggleMainMap}>看地圖</div>
                    </div>
                </section>
                <section>
                    <div className="inner-section padding-big">
                        <Swiper {...params}>
                            {slides.map((slide, index) => {
                                return (
                                    <div
                                        className="swiper-slide"
                                        style={{
                                            backgroundImage: `url(${imageUrl[index]})`,
                                        }}
                                    >
                                        <img src={imageUrl[index]} alt={slide.title}/>
                                        <div className="content">
                                            <p
                                                className="title"
                                                data-swiper-parallax="-30%"
                                                data-swiper-parallax-scale=".7"
                                            >
                                                {slide.title}
                                            </p>
                                            <span
                                                className="caption"
                                                data-swiper-parallax="-20%"
                                            >
                                                <div dangerouslySetInnerHTML={{ __html: slide.content}} />
                                            </span>
                                            <div class="actions">
                                                <div
                                                    class="open-map"
                                                    onClick={this.toggleMap}
                                                >
                                                    地圖
                                                </div>
                                                <div
                                                    class="open-pic"
                                                    onClick={() =>
                                                        this.openPic(
                                                            imageUrl[index],
                                                            slide.title
                                                        )
                                                    }
                                                >
                                                    全解析度圖片
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </Swiper>
                    </div>
                </section>
            </div>
        );
    }
}
